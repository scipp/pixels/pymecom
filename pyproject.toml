[build-system]
# Minimum requirements for the build system to execute.
requires = ["setuptools>=42.0.0", "setuptools_scm[toml]>=3.4"]
build-backend = "setuptools.build_meta"

[tool.setuptools_scm]
write_to = "src/mecom/_version.py"

[tool.pycln]
all = true

[tool.black]
line-length = 88
target-version = ['py37', 'py38']
skip-string-normalization = true
include = '\.pyi?$'
exclude = '''
/(
    \.git
  | .eggs
  | build
)/
'''

[tool.check-manifest]
ignore = [
    ".pre-commit-config.yaml",
    "mecom/*/_version.py",
]

[tool.isort]
profile = "black"

[tool.ruff.lint]
extend-select = [
  "B",        # flake8-bugbear
  "I",        # isort
  "ARG",      # flake8-unused-arguments
  "C4",       # flake8-comprehensions
  "EM",       # flake8-errmsg
  "ICN",      # flake8-import-conventions
  "G",        # flake8-logging-format
  "PGH",      # pygrep-hooks
  "PIE",      # flake8-pie
  "PL",       # pylint
  "PT",       # flake8-pytest-style
  "PTH",      # flake8-use-pathlib
  "RET",      # flake8-return
  "RUF",      # Ruff-specific
  "SIM",      # flake8-simplify
  "T20",      # flake8-print
  "UP",       # pyupgrade
  "YTT",      # flake8-2020
  "EXE",      # flake8-executable
  "NPY",      # NumPy specific rules
  "PD",       # pandas-vet
]
ignore = [
  "PLR09",    # Too many <...>
  "PLR2004",  # Magic value used in comparison
  "ISC001",   # Conflicts with formatter
]
isort.required-imports = ["from __future__ import annotations"]
# Uncomment if using a _compat.typing backport
typing-modules = ["module_qc_database_tools.typing_compat"]

[tool.ruff.lint.flake8-tidy-imports.banned-api]
"typing.Callable".msg = "Use collections.abc.Callable instead."
"typing.Iterator".msg = "Use collections.abc.Iterator instead."
"typing.Mapping".msg = "Use collections.abc.Mapping instead."
"typing.Sequence".msg = "Use collections.abc.Sequence instead."
"typing.Set".msg = "Use collections.abc.Set instead."
"importlib.abc".msg = "Use sp_repo_review._compat.importlib.resources.abc instead."
"importlib.resources.abc".msg = "Use sp_repo_review._compat.importlib.resources.abc instead."

[tool.ruff.lint.per-file-ignores]
"src/mecom/*.py" = ["E721", "ARG002", "RUF012"]

[tool.pylint]
master.py-version = "3.7"
master.ignore-paths= ["src/mecom/_version.py"]
reports.output-format = "colorized"
similarities.ignore-imports = "yes"
messages_control.disable = [
  "design",
  "fixme",
  "line-too-long",
  "wrong-import-position",
]
