"""
This file contains some custom exceptions.
"""


from __future__ import annotations


class ResponseException(Exception):
    pass


class ResponseTimeout(ResponseException):
    pass


class WrongResponseSequence(ResponseException):
    pass


class WrongChecksum(Exception):
    pass


class UnknownParameter(Exception):
    pass


class UnknownMeComType(Exception):
    pass
