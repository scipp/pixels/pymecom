from __future__ import annotations

import code
import json
import math
import time

import rich
from rich.progress import Progress
from rich.status import Status

import mecom
import mecom.mecom

mecom.mecom.TEC_PARAMETERS = [
    # included by default
    {"id": 104, "name": "Device Status", "format": "INT32"},
    {
        "id": 105,
        "name": "Error Number",
        "format": "INT32",
        "key": "error",
        "write": None,
    },
    # 3.3.1.1 Device Identification
    {"id": 106, "name": "Error Instance", "format": "INT32"},
    {"id": 107, "name": "Error Parameter", "format": "INT32"},
    {"id": 103, "name": "Firmware Version", "format": "INT32"},
    {"id": 108, "name": "Save Data to Flash", "format": "INT32"},
    {"id": 109, "name": "Flash Status", "format": "INT32"},
    {"id": 1000, "name": "Object Temperature", "format": "FLOAT32"},
    {"id": 1001, "name": "Sink Temperature", "format": "FLOAT32"},
    {"id": 1010, "name": "Target Object Temperature", "format": "FLOAT32"},
    {"id": 1011, "name": "Ramp Object Temperature", "format": "FLOAT32"},
    {"id": 1012, "name": "Thermal Power Model Current", "format": "FLOAT32"},
    {"id": 1020, "name": "Actual Output Current", "format": "FLOAT32"},
    {"id": 1021, "name": "Actual Output Voltage", "format": "FLOAT32"},
    {"id": 1030, "name": "PID Lower Limitation", "format": "FLOAT32"},
    {"id": 1031, "name": "PID Upper Limitation", "format": "FLOAT32"},
    {"id": 1032, "name": "PID Control Variable", "format": "FLOAT32"},
    {"id": 1042, "name": "Object Sensor Resistance", "format": "FLOAT32"},
    {"id": 1200, "name": "Temperature is Stable", "format": "INT32"},
    {"id": 2010, "name": "Status", "format": "INT32"},
    # 3.3.3.3 CHx Output Stage 'Static Current/Voltage' Control Values
    {"id": 2020, "name": "Set Current", "format": "FLOAT32"},
    {"id": 2021, "name": "Set Voltage", "format": "FLOAT32"},
    {"id": 2030, "name": "Current Limitation", "format": "FLOAT32"},
    {"id": 2031, "name": "Voltage Limitation", "format": "FLOAT32"},
    {"id": 2032, "name": "Current Error Threshold", "format": "FLOAT32"},
    {"id": 2033, "name": "Voltage Error Threshold", "format": "FLOAT32"},
    {"id": 2051, "name": "Device Address", "format": "INT32"},
    {"id": 3000, "name": "Target Object Temp (Set)", "format": "FLOAT32"},
    {"id": 3002, "name": "Proximity Width", "format": "FLOAT32"},
    {"id": 3004, "name": "Sine Ramp Start Point", "format": "INT32"},
    # 3.3.8.1.1 CHx Object Measurement Settings
    {"id": 6001, "name": "Current Source", "format": "INT32"},
    # 3.3.8.6.3 CHx Pump Control
    {"id": 6120, "name": "Actual Temperature Source", "format": "INT32"},
    # 3.3.8.8.5 Output Stage Controller Limit (Error 108)
    {"id": 6304, "name": "Sink Source Selection", "format": "INT32"},
    {"id": 6320, "name": "Error Delay", "format": "INT32"},
    {"id": 6300, "name": "Source Selection", "format": "INT32"},
    {"id": 6302, "name": "Observe Mode", "format": "INT32"},
    {"id": 6310, "name": "Delay till Restart", "format": "FLOAT32"},
    {"id": 50000, "name": "Live Enable", "format": "INT32"},
    {"id": 52200, "name": "External Object Temperature", "format": "FLOAT32"},
    {"id": 52201, "name": "Sink Temperature", "format": "FLOAT32"},
    # 3.3.2.8 Power Supplies and Temperature
    {"id": 1060, "name": "Driver Input Voltage", "format": "FLOAT32"},
    {"id": 1061, "name": "Medium Internal Supply", "format": "FLOAT32"},
    {"id": 1062, "name": "3.3V Internal Supply", "format": "FLOAT32"},
    {"id": 1063, "name": "Device Temperature", "format": "FLOAT32"},
    # 3.3.2.9 Device Temperature Mode (Standard or Extended)
    {"id": 1111, "name": "Maximum Output Current", "format": "FLOAT32"},
    # 3.3.3.1 CHx Output Stage Control Input Selection
    {"id": 2000, "name": "Input Selection", "format": "INT32"},
    # 3.3.4.1 CHx Nominal Temperature
    {"id": 3002, "name": "Proximity Width", "format": "FLOAT32"},
    {"id": 3003, "name": "Course Temp Ramp", "format": "FLOAT32"},
    # 3.3.4.2 CHx Temperature Controller PID Values
    {"id": 3010, "name": "Kp", "format": "FLOAT32"},
    {"id": 3011, "name": "Ti", "format": "FLOAT32"},
    {"id": 3012, "name": "Td", "format": "FLOAT32"},
    {"id": 3013, "name": "D Part Damping PT1", "format": "FLOAT32"},
    # 3.3.4.3 CHx Modelization for Thermal Power Regulation
    {"id": 3020, "name": "Mode", "format": "INT32"},
    # 3.3.4.4 CHx Peltier Characteristics
    {"id": 3030, "name": "Maximal Current Imax", "format": "FLOAT32"},
    {"id": 3033, "name": "Delta Temperature dTmax", "format": "FLOAT32"},
    {"id": 3034, "name": "Positive Current is", "format": "INT32"},
    # 3.3.5.1 CHx Object Measurement Settings
    {"id": 4001, "name": "Temperature Offset", "format": "FLOAT32"},
    {"id": 4002, "name": "Temperature Gain", "format": "FLOAT32"},
    # 3.3.5.2 CHx Actual Object Temperature Error Limits
    {"id": 4010, "name": "Lower Error Threshold", "format": "FLOAT32"},
    {"id": 4011, "name": "Upper Error Threshold", "format": "FLOAT32"},
    {"id": 4012, "name": "Max Temp Change", "format": "FLOAT32"},
    {"id": 4020, "name": "Lower Point Temperature", "format": "FLOAT32"},
    {"id": 4021, "name": "Lower Point Resistance", "format": "FLOAT32"},
    {"id": 4022, "name": "Middle Point Temperature", "format": "FLOAT32"},
    {"id": 4023, "name": "Middle Point Resistance", "format": "FLOAT32"},
    {"id": 4024, "name": "Upper Point Temperature", "format": "FLOAT32"},
    {"id": 4025, "name": "Upper Point Resistance", "format": "FLOAT32"},
    {"id": 4030, "name": "Lowest Resistance", "format": "FLOAT32"},
    {"id": 4031, "name": "Highest Resistance", "format": "FLOAT32"},
    {"id": 4032, "name": "Temperature at Lowest Resistance", "format": "FLOAT32"},
    {"id": 4033, "name": "Temperature at Highest Resistance", "format": "FLOAT32"},
    {"id": 4034, "name": "Object Sensor Type", "format": "INT32"},
    {"id": 4040, "name": "Temperature Deviation", "format": "FLOAT32"},
    {"id": 4041, "name": "Min Time in Window", "format": "FLOAT32"},
    {"id": 4042, "name": "Max Stabilization Time", "format": "FLOAT32"},
    # 3.3.6.3 CHx Sink Temperature General
    {"id": 5010, "name": "Sink Lower Error Threshold", "format": "FLOAT32"},
    {"id": 5011, "name": "Sink Upper Error Threshold", "format": "FLOAT32"},
    {"id": 5030, "name": "Sink Temperature Selection", "format": "INT32"},
    {"id": 5031, "name": "Fixed Sink Temp", "format": "FLOAT32"},
    {"id": 6005, "name": "Sensor Type Selection", "format": "INT32"},
    # 3.3.6.5 Tab: Auto Tuning
    {"id": 51000, "name": "Auto Tuning Start", "format": "INT32"},
    {"id": 51001, "name": "Auto Tuning Cancel", "format": "INT32"},
    {"id": 51002, "name": "Thermal Model Speed", "format": "INT32"},
    {"id": 51014, "name": "Tuned Kp", "format": "FLOAT32"},
    {"id": 51015, "name": "Tuned Ti", "format": "FLOAT32"},
    {"id": 51016, "name": "Tuned Td", "format": "FLOAT32"},
    {"id": 51020, "name": "Auto Tuning Status", "format": "INT32"},
    {"id": 51021, "name": "Tuning Progress", "format": "FLOAT32"},
    # 3.3.8.1 Power Supply Parameters (Bus-Controlled) Mode Parameters
    {"id": 50001, "name": "Live Set Current", "format": "FLOAT32"},
    {"id": 50002, "name": "Live Set Voltage", "format": "FLOAT32"},
]

error_names = {
    # processor errors
    "0": "No error",
    "1": "MCU system malfunction",
    "2": "MCU system malfunction",
    "3": "MCU system malfunction",
    "4": "MCU system malfunction",
    "5": "MCU system malfunction",
    "6": "MCU system malfunction",
    "7": "MCU system malfunction",
    "8": "MCU system malfunction",
    "9": "MCU system malfunction",
    "10": "MCU system malfunction",
    # HMI errors
    "11": "Emergency stop was triggered by LTR1200",
    "12": "LTR-1200 HMI regularly sends free signals to all rack-internal devices such that they are allowed to activate their output stages (if enabled)--no signal received for >1sec",
    # param system errors
    "20": "Internal parameter system malfunction",
    "21": "Internal parameter system malfunction",
    "22": "Parameter set corrupt",
    "23": "Parameter set incompatible with current firmware version",
    "24": "Firmware does not recognize valid device",
    "25": "Internal parameter system malfunction",
    "26": "Internal limit system malfunction",
    "27": "Parameter write or read wrong datatype function used",
    "28": "Parameter write value out of range",
    "29": "Parameter save to flash called from interrupt",
    # power supply errors
    "30": "Input voltage net too low",
    "31": "Input voltage net too high",
    "32": "Internal Medium Voltage power net too low",
    "33": "Internal Medium Voltage power net too high",
    "36": "Internal 3.3V power net too low<3.1V",
    "37": "Internal 3.3V power net too high>3.5V",
    # flash memory errors
    "50": "On-board flash failure-write timeout",
    "51": "On-board flash failure-erase timeout",
    "52": "On-board flash failure-invalid address",
    # communication (UART) error
    "53": "Send buffer overflow error",
    # device temp and hardware errors
    "60": "Device running too hot",
    "61": "Communication error with I/O hardware during factory test",
    # power output
    "100": "Overcurrent, Driver CHx OUT+",
    "101": "Overcurrent, Driver CHx OUT+",
    "102": "Overcurrent, Driver CHx OUT-",
    "103": "Overcurrent, Driver CHx OUT-",
    "104": "Overvoltage, Driver CHx OUT+",
    "105": "Overvoltage, Driver CHx OUT+",
    "106": "Residual current too high. The Current difference between OUT+ and OUT- is too big.",
    "107": "Overall current monitoring, triggers fast switch off (within 10 μs)",
    "108": "Output Stage saturation error. Check input current is sufficient and Vout not set too close to Vin. Try to reduce the Current Limitation in the Operation tab.",
    "109": "Currents through OUT+ and OUT- too unequal",
    "110": "TEC Power Output Error: Allowed total output power reached",
    "111": "The connected load has a too low resistance in comparison to the input voltage",
    # current measurement
    "120": "Offset during initialization of current monitor too high, Driver CHx OUT+",
    "121": "Offset during initialization of current monitor too low, Driver CHx OUT+",
    "122": "Offset during initialization of current monitor too high, Driver CHx OUT+",
    "123": "Offset during initialization of current monitor too low, Driver CHx OUT+",
    # object temp
    "130": "Object Temperature Measurement Circuit Initialization failure.",
    "131": "Object Temperature Measurement Circuit failure.",
    "132": "External ADC supply voltage out of range",
    "133": "23bit ADC raw value below safety margin",
    "134": "23bit ADC raw value above safety margin or the measured resistance is too high.",
    "137": "Measured object temperature out of permitted range",
    "138": "Measured object temperature out of permitted range",
    "139": "Change in measured object temperature too fast (outpacing thermal inertia)",
    "150": "Object Temperature Measurement Circuit failure.",
    "151": "Object Temperature Measurement Circuit failure.",
    "152": "Object Temperature Measurement Circuit failure.",
    "153": "Object Temperature Measurement Circuit failure.",
    # sink temp
    "140": "12bit ADC raw value below safety margin",
    "141": "12bit ADC raw value above safety margin",
    "142": "Measured sink temperature too low",
    "143": "Measured sink temperature too high",
    "144": "Change in measured sink temperature too fast (outpacing thermal inertia)",
    # autotune errors
    "170": "Progress Error",
    "171": "Auto tuning failures at three consecutive attempts",
    "172": "Auto tuning failures at three consecutive attempts",
    "173": "The Temperature Controller is in its limitation or is not running.",
    # fan control
    "175": "The Fan does not reach the desired rotation speed.",
    "176": "The Fan does not rotate",
    # lookup errors
    "180": "Unknown Instruction",
    "181": "Misuse of an Instruction",
    # misc
    "182": "Temperature Stability not reached in specified time.",
    "183": "No package has been received within the specified Watchdog timeout time.",
    "184": "Syntax Error in Display Format Argument String",
}

# status names corresponding to driver status (mc[1080])
device_status = {
    "0": "Init",
    "1": "Ready",
    "2": "Run",
    "3": "Error",
    "4": "Bootloader",
    "5": "Device will Reset within the next 200ms",
}

input_modes = {
    "0": "Static current/voltage input",
    "1": "Live current/voltage input",
    "2": "Temp controller input",
}
temp_stability = {
    "0": "Temp regulation not active",
    "1": "Not stable",
    "2": "Stable",
}
output_status = {
    "0": "Static OFF",
    "1": "Static ON",
    "2": "Live OFF/ON",
    "3": "HW enable",
}

name_dicts = {
    "104": device_status,
    "105": error_names,
    "1200": temp_stability,
    "2000": input_modes,
    "2010": output_status,
}


def build_meerstetter_sync(properties):
    def make_getter(addr):
        def getter(self):
            return self.get_parameter(parameter_id=addr, address=self.identify())

        if addr:
            return getter
        return None

    def make_setter(addr):
        if addr is None:
            return None
        return lambda self, value: self.__setitem__(addr, value)

    namespace = {
        prop["key"]: property(
            fget=make_getter(prop["id"]), fset=make_setter(prop["id"])
        )
        for prop in properties
        if "key" in prop
    }
    return type("MeerstetterBase", (mecom.MeCom,), namespace)


"""
def build_meerstetter_async(properties):
    def make_getter(addr):
        if addr is None:
            return None

        def getter(self):
            return asyncio.to_thread(
                partial(get_parameter, parameter_id=addr, address=self.address)
            )

        return getter

    def make_setter(addr):
        if addr is None:
            return None

        def setter(self, value):
            return asyncio.to_thread(
                partial(
                    set_parameter, parameter_id=addr, address=self.address, value=value
                )
            )

        return setter

    namespace = {
        prop["key"]: property(
            fget=make_getter(prop["id"]), fset=make_setter(prop["id"])
        )
        for prop in properties
        if "key" in prop
    }
    return type("MeerstetterBase", (), namespace)
"""

if __name__ == "__main__":
    MeerstetterBase = build_meerstetter_sync(mecom.mecom.TEC_PARAMETERS)
    with MeerstetterBase("/dev/ttyUSB3") as mb:
        rich.print("finished mb")


class Peltier(mecom.MeCom):
    def __init__(self, port, interesting_ids=None):
        super().__init__(port)
        self.interested = interesting_ids or [
            104,
            105,
            1000,
            1001,
            1010,
            1020,
            1021,
            1200,
            2000,
            2010,
        ]
        self.statusbar = None

    @property
    def error(self):
        # return self[105]
        error_name = error_names.get(str(self[105]))
        error_name = f" ({error_name})" if error_name else ""
        return f"{self[105]}{error_name}"

    def summary(self, verbose=False):
        for command in sorted(self.PARAMETERS._PARAMETERS, key=lambda x: x.id):
            if verbose or command.id in self.interested:
                value = self[command.id]
                value_str = "<unknown>"
                value_name = "<unknown>"
                if value is not None:
                    value_name = name_dicts.get(str(command.id), {}).get(str(value))
                    value_str = (
                        f"{value:0.4f}" if isinstance(value, float) else str(value)
                    )
                    if value_name is None:
                        value_name = value_str
                        value_str = ""
                msg = f"({command.id:05d}){command.name:40s}{value_name:40s}{value_str}"
                rich.print(msg)
                time.sleep(1)

    def enable(self):
        self[2010] = 1

    def disable(self):
        self[2010] = 0

    @property
    def port(self):
        return self.serial.port

    @property
    def status(self):
        try:
            return super().status()
        except mecom.exceptions.ResponseTimeout:
            return "Offline"

    @property
    def address(self):
        return super().identify()

    def __getitem__(self, parameter_id):
        try:
            value = super().get_parameter(
                parameter_id=parameter_id, address=self.address
            )
        except (mecom.ResponseException, mecom.WrongChecksum):
            self.stop()
            _ids = self.interested
            _parameters = self.PARAMETERS._PARAMETERS
            super().__init__(self.port)
            self.interested = _ids
            self.PARAMETERS._PARAMETERS = _parameters
            value = None
        return value

    def __setitem__(self, parameter_id, value):
        try:
            super().set_parameter(
                parameter_id=parameter_id, value=value, address=self.address
            )
        except (mecom.ResponseException, mecom.WrongChecksum):
            self.stop()
            _ids = self.interested
            _parameters = self.PARAMETERS._PARAMETERS
            super().__init__(self.port)
            self.interested = _ids
            self.PARAMETERS._PARAMETERS = _parameters

    def __repr__(self):
        """Representation of the object"""
        module = type(self).__module__
        qualname = type(self).__qualname__
        return f"<{module}.{qualname}(port={self.port}, addr={self.address}, status={self.status}) at {hex(id(self))}>"

    @property
    def object_temp(self):
        return self[1000]

    @property
    def object_resistance(self):
        return self[1042]

    @property
    def sink_temp(self):
        return self[1001]

    @property
    def input_mode(self):
        return self[2000]

    @input_mode.setter
    def input_mode(self, mode):
        # 0: Static Current/Voltage (Uses ID 2020...)
        # 1: Live Current/Voltage (Uses ID 50001...)
        # 2: Temperature Controller
        self[2000] = mode

    @property
    def target_temp(self):
        return self[1010]

    @target_temp.setter
    def target_temp(self, tempTarget):
        self[3000] = float(tempTarget)

    def _compress_send(self, client, topic, value, timestamp):
        if value is None or math.isnan(value):
            return
        msg = json.dumps(
            {
                "value": value,
                "time": timestamp,
                "tags": {
                    "make": "meerstetter",
                    "model": "TEC-1089-SV-NTC",
                },
            }
        )
        client.publish(topic, msg)

    def reset_device(self):
        with Status("[red]Resetting device...[/]") as self.statusbar:
            super().reset_device()
            while self.status != "Ready":
                self.statusbar.update(f"Status = [yellow]{self.status}[/]")

            rich.print("Device has been reset.")
            self.statusbar.stop()

    def dbUpload(self, client, topic):
        timestamp = int(time.time())
        if self.object_temp and float(self.object_temp) != -100.0:
            self._compress_send(
                client, f"{topic}module/temperature", self.object_temp, timestamp
            )
            self._compress_send(
                client, f"{topic}module/resistance", self.object_resistance, timestamp
            )
        current, voltage = self.current, self.voltage
        self._compress_send(client, f"{topic}peltier/current", current, timestamp)
        self._compress_send(client, f"{topic}peltier/voltage", voltage, timestamp)
        self._compress_send(client, f"{topic}peltier/reg1030", self[1030], timestamp)
        self._compress_send(client, f"{topic}peltier/reg1031", self[1031], timestamp)
        self._compress_send(client, f"{topic}peltier/reg1032", self[1032], timestamp)
        return current, voltage

    def to_temp(self, tempTarget, client, topic, callback=None):
        try:
            self.target_temp = tempTarget
            with Status(
                f"Temp = [yellow]{self.object_temp:0.2f}\u00B0C[/yellow] ({self.object_resistance:0.2f} Ohm) (Unstable)\n Sink = [red] {self.sink_temp:0.2f}\u00B0C[/red]\n Volt = [yellow]{self.voltage:0.2f}V[/]\n Curr = [yellow]{self.current:0.2f}A[/yellow]"
            ) as self.statusbar:
                self.enable()
                if callable(callback):
                    callback(self)
                else:
                    self.dbUpload(client=client, topic=topic)
                time.sleep(1.0)  # wait for register to update status!
                while self.stable != 2:
                    time.sleep(1.0)
                    self.dbUpload(client=client, topic=topic)
                    self.statusbar.update(
                        f"Temp = [yellow]{self.object_temp:0.2f}\u00B0C[/yellow] (Unstable)\n Sink = [red]{self.sink_temp:0.2f}\u00B0C[/red]\n Curr = [yellow]{self.current:0.2f}A[/yellow]"
                    )

                rich.print(f"Stability reached at {tempTarget}.")
                self.statusbar.stop()
        except KeyboardInterrupt:
            self.disable()

    @property
    def current(self):
        return self[1020]

    @current.setter
    def current(self, current):
        self[2020] = float(current)

    @property
    def sink_low_temp(self):
        return self[5043]

    @sink_low_temp.setter
    def sink_low_temp(self, sink_low_temp):
        self[5043] = float(sink_low_temp)

    def read_current(self):
        try:
            with Status(f"Current = [green]{self.current:0.5f}A[/green]") as status:
                while True:
                    time.sleep(0.5)
                    status.update(f"Current = [green]{self.current:0.5f}A[/green]")
        except KeyboardInterrupt:
            pass

    def read_voltage(self):
        try:
            with Status(f"Voltage = [green]{self.voltage:0.5f}V[/green]") as status:
                while True:
                    time.sleep(0.5)
                    status.update(f"Voltage = [green]{self.voltage:0.5f}V[/green]")
        except KeyboardInterrupt:
            pass

    def read_power(self):
        try:
            with Status(
                f"Current = [green]{self.current:0.5f}A[/green]\n  Voltage = [green]{self.voltage:0.5f}V[/green]"
            ) as status:
                while True:
                    time.sleep(0.5)
                    status.update(
                        f"Current = [green]{self.current:0.5f}A[/green]\n  Voltage = [green]{self.voltage:0.5f}V[/green]"
                    )
        except KeyboardInterrupt:
            pass

    def read_temp(self):
        try:
            with Status(
                f"Obj Temp = [green]{self.object_temp:0.5f}C[/green]\nSink Temp = [red]{self.sink_temp:0.5f}C[/red]"
            ) as status:
                while True:
                    time.sleep(0.5)
                    status.update(
                        f"Obj Temp = [green]{self.object_temp:0.5f}C[/green]\nSink Temp = [red]{self.sink_temp:0.5f}C[/red]"
                    )
        except KeyboardInterrupt:
            pass

    @property
    def current_limit(self):
        return self[2030]

    @current_limit.setter
    def current_limit(self, current):
        self[2030] = float(current)

    @property
    def voltage(self):
        return self[1021]

    @voltage.setter
    def voltage(self, voltage):
        self[2021] = float(voltage)

    @property
    def voltage_limit(self):
        return self[2031]

    @voltage_limit.setter
    def voltage_limit(self, voltage):
        self[2031] = float(voltage)

    @property
    def live_current(self):
        return self[50001]

    @live_current.setter
    def live_current(self, current):
        self[50001] = float(current)

    @property
    def live_voltage(self):
        return self[50002]

    @live_voltage.setter
    def live_voltage(self, voltage):
        self[50002] = float(voltage)

    @property
    def prox_width(self):
        return float(self[3002])

    @property
    def course_temp_ramp(self):
        return self[3003]

    @property
    def stable(self):
        return self[1200]

    @property
    def tuning_status(self):
        _mapping = {
            0: "Idle",
            1: "Ramping to Target Temp.",
            2: "Preparing for Acq.",
            3: "Acquiring Data",
            4: "[green]Success[/green]: Tuning Complete!",
            10: f"[red]Error[/red]: {self.error}",
        }
        _status = self[51020]
        return _mapping.get(_status)

    def set_pid_parameters(self, kp=10.0, ki=300.0, kd=0.0):
        rich.print(f"Setting PID parameters: Kp={kp:0.2f}, Ki={ki:0.2f}, Kd={kd:0.2f}")
        self[3010] = kp
        self[3011] = ki
        self[3012] = kd

    def auto_tune(self, target_temp=15.0):
        assert self.status == "Ready", "Must be in 'Ready' state"

        # set standard PID settings
        rich.print("Setting standard PID settings")
        self.set_pid_parameters()

        # set target temperature
        self.target_temp = target_temp

        # and enable
        rich.print("Turning on TEC")
        self.enable()
        # self.dbUpload()

        while self.stable != 2:
            rich.print(
                f"Status: {self.status}. Stable? {self.stable}. Object Temp: {self.object_temp:.2f}C. Tuning Status: {self[51020]}"
            )

            # self.dbUpload()
            time.sleep(0.5)

        rich.print(f"Stabilized at {self.object_temp}")

        # start auto-tuning
        # set fast thermal model speed
        self[51002] = 0

        # start the auto-tuning
        self[51000] = 1

        with Progress(refresh_per_second=2) as progress:
            task_tuning = progress.add_task(
                f"[red]Auto-tuning[/red] ({self.tuning_status})...", total=100
            )
            while True:
                completed = self[51021]
                progress.update(
                    task_tuning,
                    description=f"[red]Auto-tuning ({self.tuning_status})...",
                    completed=completed,
                )
                # self.dbUpload()
                if completed == 100.0:
                    break
                time.sleep(1)

        rich.print("Tuning procedure done.")
        rich.print(f" - Kp: {self[3010]:0.2f} -> {self[51014]:0.2f}")
        rich.print(f" - Ti: {self[3011]:0.2f} -> {self[51015]:0.2f}")
        rich.print(f" - Td: {self[3012]:0.2f} -> {self[51016]:0.2f}")

        rich.print("Turning off TEC")
        self.disable()

        self.set_pid_parameters(self[51014], self[51015], self[51016])

    def to_static(self, current):
        self[2000] = 0
        self[2020] = current
        self[2010] = 1
        rich.print("Static mode ENABLED, current running!")

    def to_temp_control(self):
        self[2000] = 2
        self[2010] = 0
        rich.print("Temperature controller enabled.")

    def wait_until_ready(self):
        status = self.status
        while status != "Ready":
            if status == "Run":
                mc.disable()
            time.sleep(1)
            status = self.status


if __name__ == "__main__":
    with Peltier("/dev/ttyUSB3") as mc:
        try:
            # # which device are we talking to?
            address = mc.address
            status = mc.status

            while status != "Ready":
                rich.print(f"Status is: {status}")
                rich.print(f"Error is: {mc.error}")
                if status == "Run":
                    mc.disable()
                mc.reset_device()
                time.sleep(3)
                status = mc.status

            rich.print(f"Status is: {status}")

            # set PS settings
            mc.voltage_limit = 15.0
            mc.current_limit = 8.0

            mc[2000] = 1  # live
            mc[50001] = 2.0

            rich.print(mc)
            rich.print("-" * 50)
            mc.summary(True)

            code.interact(local=locals())

            rich.print("done")

        except KeyboardInterrupt:
            mc.disable()
            mc.reset_device()
