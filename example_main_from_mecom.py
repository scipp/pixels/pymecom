from __future__ import annotations

from mecom import MeCom

with MeCom("/dev/ttyUSB0") as mc:
    # # which device are we talking to?
    address = mc.identify()
    status = mc.status()
    print(f"connected to device: {address}, status: {status}")

    # get object temperature
    temp = mc.get_parameter(parameter_name="Object Temperature", address=address)
    print(f"query for object temperature, measured temperature {temp}C")

    # is the loop stable?
    stable_id = mc.get_parameter(
        parameter_name="Temperature is Stable", address=address
    )
    if stable_id == 0:
        stable = "temperature regulation is not active"
    elif stable_id == 1:
        stable = "is not stable"
    elif stable_id == 2:
        stable = "is stable"
    else:
        stable = "state is unknown"
    print(f"query for loop stability, loop {stable}")

    # # setting a new device address and get again
    # new_address = 6
    # value_set = mc.set_parameter(value=new_address, parameter_name="Device Address")
    # print("setting device address to {}".format(value_set))
    #
    # # get device address again
    # address = mc.identify()
    # print("connected to device: {}".format(address))

    # set target temperature to 21C
    # success = mc.set_parameter(value=20.0, parameter_id=3000)
    # print(success)

    print("leaving with-statement, connection will be closed")
